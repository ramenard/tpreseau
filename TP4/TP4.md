# I. First steps
Pour Spotify
```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.33.16.43:8852       104.18.42.171:443      ESTABLISHED     InHost
```
Il n'y a qu'une partie ici mais on peut voir la meme connection au port 8640 qu'on peut voir plus en détail [ici](assets/TCPSpotify.pcapng)

Pour Visual Studio Code
```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.33.16.43:8893       13.107.5.93:443      ESTABLISHED     InHost
```
Pour voir plus en détail [ici](assets/TCPVisualStudioCode.pcapng)

Pour Discord
```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.33.16.43:1024       162.159.134.234:443      ESTABLISHED     InHost
```
Pour voir plus en détail [ici](assets/TCPDiscord.pcapng)

Pour le client Riot
```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.33.16.43:1081       162.247.241.1:443      ESTABLISHED     InHost
```
Pour voir plus en détail [ici](assets/TCPRiot.pcapng)

Pour Firefox
```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.33.16.43:1223       34.117.237.239:443      ESTABLISHED     InHost
```
Pour voir plus en détail [ici](assets/TCPFirefox.pcapng)

# II. Mise en place 

## 1 SSH

On peut voir [ici](assets/TCPSsh.pcap) que la connexion en ssh se fait sur le port 22 en tcp
Je sais pas pourquoi mais j'ai aucun FIN ACK qui apparait alors que j'attends bien que la connexion en ssh s'arrete pour arreter le tcpdump

```bash
netstat -tn

Connexion actives

  Proto  Adresse locale          Adresse distante      État            État de déchargement

  TCP    10.3.1.1:1629          10.3.1.11:22           ESTABLISHED     InHost
```

### 3 DNS