# I ARP
## 1 Echange ARP

Ici le ping de Jhon à Marcel

```bash
ping 10.3.1.12

PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.553 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.664 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.529 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.853 ms

--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3098ms
rtt min/avg/max/mdev = 0.529/0.649/0.853/0.127 ms
```

Et ici le ping de Marcel à Jhon

```bash
ping 10.3.1.11

PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=0.455 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.974 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.894 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.902 ms

--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3050ms
rtt min/avg/max/mdev = 0.455/0.806/0.974/0.205 ms
```

Depuis Marcel,
```bash
ip neigh show

10.3.1.11 dev enp0s3 lladdr 08:00:27:38:ee:4b REACHABLE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:11 REACHABLE
```
L'adresse MAC de Jhon est 08:00:27:38:ee:4b

Depuis Jhon,
```bash
ip neigh show

10.3.1.12 dev enp0s3 lladdr 08:00:27:ff:22:0d REACHABLE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:11 REACHABLE
```
L'adresse MAC de Marcel est 08:00:27:ff:22:0d

```bash
ip address show enp0s3

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
xxx link/ether 08:00:27:ff:22:0d brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feff:220d/64 scope link
       valid_lft forever preferred_lft forever
```
On peut voir en face des x l'adresse MAC de Marcel qu'on retrouve bien dans la table ARP de Jhon

## 2 Analyses de Trames

La premiere étape est de vider sa table ARP grâce à la commande suivante : 
```bash
sudo ip neigh flush all
```

```bash
ip neigh show

10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:11 REACHABLE
```

Maintenant que la table ARP est vide on ping depuis l'ordianteur de Marcel jusqu'a celui de Jhon

```bash
ping 10.3.1.12

PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=0.711 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.891 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=0.856 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=0.927 ms
```

Et pendant ce temps sur l'autre machine on va effectuer une capture de trames avec la commande suivante :
```bash
sudo tcpdump -i enp0s8 -w tp3_arp.pcap not port 22
```

On peut voir les ping sur le document juste [ici](assets/tp3_arp.pcap)

# II Routage
## 1. Mise en place du routage

La premiere chose à faire est activer le routage :

```bash
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```bash
sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8
```

```bash
sudo firewall-cmd --add-masquerade --zone=public
success
```

```bash
sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

Il faut aussi rajouter l'adresse ip du router comme adresse passerelle pour Jhon et Marcel
dans le dossier ifcfg-enp0s3
```bash
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS1=1.1.1.1
```
Comme on peut le voir ici

```bash
ping 10.3.2.12

PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.67 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.57 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=2.09 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=0.731 ms
^C
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 0.731/1.514/2.088/0.492 ms
```
Ici on peut voir que le ping de Jhon à Marcel fonctionne

```bash
ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:11 REACHABLE
10.3.1.254 dev enp0s3 lladdr 08:00:27:c2:e1:3c STALE
```
On peut voir que seulement l'adresse IP de la passerelle apparé sur la table ARP

```bash
ip neigh show
10.3.2.1 dev enp0s3 lladdr 0a:00:27:00:00:06 REACHABLE
10.3.2.254 dev enp0s3 lladdr 08:00:27:b0:1b:f0 REACHABLE
```
On peut constater la meme chose du coté de Marcel

```bash
ip neigh show
10.3.2.12 dev enp0s8 lladdr 08:00:27:ff:22:0d STALE
10.3.1.11 dev enp0s3 lladdr 08:00:27:38:ee:4b STALE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:11 DELAY
```
On peut voir cepandant que du coté du router, l'adresse de Jhon et de Marcel apparaisse

Je pense que ce qu'il se passe c'est que pour ping de Jhon à Marcel, Jhon ping d'abord le router en passant par la passerelle puis ensuite la passerelle ping Marcel

## 2. Analyse de trames
| Ordre | Type Trame | IP source | MAC Source | IP Destiantion | MAC Destination |
|:----------|:-------------:|:------:|:----------:|:-------------:|:------:|
| 1 | ARP | 10.3.2.254 | root 08:00:27:b0:1b:f0 | 255.255.255.255 | Broadcast ff:ff:ff:ff:ff:ff |
| 2 | ARP | 10.3.2.12 | marcel 08:00:27:ff:22:0d | 10.3.2.254 | root 08:00:27:b0:1b:f0 |
| 3 | ICMP | 10.3.2.254 | root 08:00:27:b0:1b:f0 | 10.3.2.12 | marcel 08:00:27:ff:22:0d |
| 4 | ICMP | 10.3.2.12 | marcel 08:00:27:ff:22:0d | 10.3.2.254 | root 08:00:27:b0:1b:f0 |

Pour plus de détail voir [ici](assets/tp2_routage_marcel.pcap)

## 3. Accès internet
Pour savoir si Jhon ou Marcel a un accés à Internet on peut ping vers un serveur DNS connu comme 8.8.8.8

```bash
ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=84.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=22.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=17.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=16.9 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 16.894/35.351/84.895/28.679 ms
```
Ici on peut voir que les pings fonctionnent bien
| Ordre | Type Trame | IP source | MAC Source | IP Destiantion | MAC Destination |
|:----------|:-------------:|:------:|:----------:|:-------------:|:------:|
| 1 | ping | 10.3.1.11 | jhon 08:00:27:38:ee:4b | 8.8.8.8 | 08:00:27:c2:e1:3c |
| 2 | pong | 8.8.8.8 | 08:00:27:c2:e1:3c | 10.3.1.11 | jhon 08:00:27:38:ee:4b |
| 3 | UDP | 8.8.8.8 | 08:00:27:c2:e1:3c | 10.3.1.11 | jhon 08:00:27:38:ee:4b |

Pour plus de détail voir [ici](assets/tp2_routage_internet.pcapng)

# III DHCP
