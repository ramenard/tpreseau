I1

Commande pour trouver son ip
```bash
ipconfig /all
```

Pour la carte réseau :
```bashCarte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Adresse physique . . . . . . . . . . . : 3C-9C-0F-EC-51-03
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.87(préféré)
```

J'ai pas de carte Ethernet :(

La passerelle :
```bash
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
```
![image](assets/IPv4.PNG)
![image](assets/MAC.PNG)

![image](assets/Passerelle.PNG)

La gateway dans le réseau d'YNOV nous sert à se connecter à d'autres Lan.

2A

```bash
Carte réseau sans fil Wi-Fi :

   Adresse IPv4. . . . . . . . . . . . . .: 10.33.19.200
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
```
Si on perd internet c'est parce qu'on a pris une adresse IP déjà adressé à quelqu'un et c'est le premier connecté qui a la prioritée.

II

3
```bash
Carte Ethernet Ethernet :

   Adresse IPv6 de liaison locale. . . . .: fe80::3056:5918:de36:d2bb%59(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
```
```bash
ping 192.168.0.2

Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
```


```bash
Interface : 192.168.0.1 --- 0x3b
  Adresse Internet      Adresse physique      Type
  192.168.0.2           00-e0-4c-68-00-67     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```
On peut voir l'adresse MAC de la personne à qui je suis connecté en face de son ip
grâce à la commande : arp -a

4
```bash
tracert 8.8.8.8

Détermination de l’itinéraire vers 8.8.8.8 avec un maximum de 30 sauts.

  1    <1 ms     *        1 ms  DESKTOP-JA0JAN6 [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms     6 ms     6 ms  10.33.19.254
  4    12 ms    11 ms     6 ms  77.196.149.137
  5    12 ms    11 ms    11 ms  212.30.97.108
  6    24 ms    23 ms    23 ms  77.136.172.222
  7    24 ms    22 ms    21 ms  77.136.172.221
  8    33 ms    48 ms    30 ms  194.6.144.186
  9    33 ms    25 ms    23 ms  194.6.144.186
 10    24 ms    24 ms    25 ms  72.14.194.30
 11    26 ms    26 ms    24 ms  172.253.69.49
 12    24 ms    24 ms    25 ms  108.170.238.107
 13    24 ms    25 ms    24 ms  8.8.8.8

Itinéraire déterminé.
```
On peut voir que sur le PC qui n'est pas connecté à internet, il peut quand même ping 8.8.8.8 grâce à l'autre PC

5

Voici la commande pour le côté serveur
```bash
.\nc.exe -l -p 8888
```

Ici pour le côté client
```bash
.\nc.exe 192.168.137.2 8888
Bonjour, Moi c'est Raphaël Menard le client de la B2B eleve à l'ecole ynov
Salut ! Moi c'est Fabien GARCIA le serveur :D
Enchanté Fabien GARCIA le serveur !!!
```


```bash
.\nc.exe -l -p 8888 -s 192.168.137.2
```
Cette commande permet d'écouter sur l'ip 192.168.137.2 sur le port 8888


6
```bash
netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
```
Cette commande permet d'autoriser les paquets ICMP de type 8 entrant (les pings)


```bash
ping 10.33.19.104

Envoi d’une requête 'Ping'  10.33.19.104 avec 32 octets de données :
Réponse de 10.33.19.104 : octets=32 temps=4 ms TTL=128
Réponse de 10.33.19.104 : octets=32 temps=5 ms TTL=128
Réponse de 10.33.19.104 : octets=32 temps=30 ms TTL=128
Réponse de 10.33.19.104 : octets=32 temps=31 ms TTL=128

Statistiques Ping pour 10.33.19.104:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 4ms, Maximum = 31ms, Moyenne = 17ms
```


```bash
netsh advfirewall firewall add rule name= "Open Port 8888" dir=in action=allow protocol=TCP localport=8888
```
Cette commande permet d'autoriser les connexions TCP entrante sur le port 8888



```bash
.\nc.exe -l -p 8888
bonjour
```
Même avec le pare-feu d'activer on peut quand même ping et discuter 


III
1

En utilisant la commande ipconfig /all on peut voir le bail et le serveur DHCP
```bashCarte réseau sans fil Wi-Fi :

   Bail obtenu. . . . . . . . . . . . . . : mercredi 28 septembre 2022 08:55:18
   Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 08:55:18
   Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
```

2
Toujours grâce à ipconfig /all
```bashCarte réseau sans fil Wi-Fi :

   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

```bash
nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:808::200e
          216.58.215.46
```

```bash
nslookup ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Addresses:  2606:4700:20::681a:be9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          172.67.74.226
          104.26.11.233
          104.26.10.233
```
La commande passe deja par le dns de google qui lui envoie en réponse l'ip de google.com et de ynov.com
Addresses correspond à l'adresse ip du réseau qui lui est attribué
Si il y en a plusieurs c'est qu'il y a plusieurs serveurs

```bash
nslookup 78.74.21.21
Serveur :   bbox.lan
Address:  2001:861:51:d720:3224:78ff:fe21:557c

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```
On peut voir ici que cette adresse ip correspond à host-78-74-21-21.homerun.telia.com


```bash
nslookup 92.146.54.88
Serveur :   bbox.lan
Address:  2001:861:51:d720:3224:78ff:fe21:557c

*** bbox.lan ne parvient pas à trouver 92.146.54.88 : Non-existent domain
```
On peut voir ici que cette adresse ip est associé à aucun site

IV

![image](assets/pingPasserelle.PNG)
Sur cette image on peut voir d'abord un ping demon ordi à la passerelle

Et juste en-dessous une réponse qui va de la passerelle à mon ordi

![image](assets/netCatMate1.PNG)
Sur cette image on peut voir un message envoyé depuis mon ordi à celui de mon mate gràce à netcat

![image](assets/netCateMate2.PNG)

On peut voir ici, surligné en bleu, le message envoyé d'un ordi à l'autre

![image](assets/DNSrequest.PNG)
On peut voir une requête DNS, la premiere IPV6 correspondant à celle de mon ordi et la deuxieme correspondant à celle à qui je pose la question. Il s'agit de l'adresse du serveur DNS de ma box

Merci de ta lecture.